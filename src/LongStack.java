import java.util.LinkedList;

public class LongStack {

   public static void main (String[] argum) {
      Long res = interpret("1    32    15 * - + ");
      System.out.println(res);
   }

   private LinkedList<Long> slist;
   LongStack() {
      slist = new LinkedList<Long>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      LongStack tmp = new LongStack();
      if (this.slist.size() >= 0)
         for (int i = 0; i < this.slist.size(); i++)
            tmp.slist.add(this.slist.get(i));
      return tmp;
   }

   public boolean stEmpty() {
      return (slist.size() == 0);
   }

   public void push (long a) {
      slist.add(a);
   }

   public long pop() {
      if (stEmpty())
         throw new RuntimeException("Avaldises on liiga vähe arve.");
      return slist.removeLast();
   }

   public void op (String s) {
      //Used example "intStack" introduced in lecture
      if (slist.size() < 2)
         throw new IndexOutOfBoundsException(" too few elements for " + s);
      Long op2 = pop();
      Long op1 = pop();
      if (s.equals("+"))
         push(op1 + op2);
      else if (s.equals("-"))
         push(op1 - op2);
      else if (s.equals("*"))
         push(op1 * op2);
      else if (s.equals("/"))
         push(op1 / op2);
      else
         throw new IllegalArgumentException("Invalid operation: " + s);
   }
  
   public long tos() {
      if (stEmpty())
         throw new RuntimeException("Avaldises on liiga vähe arve.");
      return slist.getLast();
   }

   @Override
   public boolean equals (Object o) {
      if (((LongStack) o).slist.size() != this.slist.size())
         return false;
      for (int i = 0; i < this.slist.size(); i++) {
         if (((LongStack) o).slist.get(i) != this.slist.get(i))
            return false;
      }
      return true;
   }

   @Override
   public String toString() {
      if (this.stEmpty())
         return "Stack is empty.";
      StringBuffer outputStr = new StringBuffer();
      for (int i = 0; i < this.slist.size(); i++) {
         outputStr.append(String.valueOf(slist.get(i) + " "));
      }
      return outputStr.toString();
   }

   public static long interpret (String pol) {
      /*
      Esialgne puudulik idee: Lugeda string tükkhaaval, tükkide eraldajaks tühikud. Kui "tükk" on teisendatav pikaks täisarvuks, lisame magasini.
      Kui "tükk" on üks tehtemärkidest +-/*, kogume väärtused jadamisi eraldi stringi. Muul juhul anname erindi.
      */
      /*
      Idee täiendus: arvud ei pruugi jadas olla rangelt ees ja operatsioonid rangelt taga.
      */
      String origPol = pol;

      if (pol.length() == 0)
         throw new RuntimeException("Avaldis ei tohi olla tühi");

      //available operations
      String operations = "+-*/";

      //initiate stack
      LongStack stack = new LongStack();

      //for collecting operations from input expression
      String opStr = "";

      //split expression and calculate
      pol = pol.trim();
      while (pol.contains(" ")) {
         String str = pol.substring(0,pol.indexOf(" "));
         pol = pol.substring(pol.indexOf(" "),pol.length()).trim();
         try {
            Long l = Long.parseLong(str);
            stack.push(l);
         }
         catch (NumberFormatException e){
            if (operations.contains(str))
               opStr = str;
            else throw new RuntimeException(String.format("Avaldis %s sisaldab keelatud sümboleid.","'"+origPol+"'"));
            if (stack.slist.size() > 1)
               stack.op(opStr);
            else
               throw new RuntimeException(String.format("Avaldis %s ei vasta pööratud poola kujule","'"+origPol+"'"));
         }
         if (opStr.length() > stack.slist.size())
            throw new RuntimeException(String.format("Avaldis {pol} ei vasta pööratud poola kujule","'"+origPol+"'"));
      }

      try {
         Long l = Long.parseLong(pol);
         stack.push(l);
      }
      catch (NumberFormatException e){
         if (operations.contains(pol))
            opStr = pol;
         else throw new RuntimeException(String.format("Avaldis %s sisaldab keelatud sümboleid.","'"+origPol+"'"));
         if (stack.slist.size() > 1)
            stack.op(opStr);
         else
            throw new RuntimeException(String.format("Avaldis %s ei vasta pööratud poola kujule","'"+origPol+"'"));
      }

       if (stack.slist.size() > 1)
          throw new RuntimeException(String.format("Avaldises %s on operatsioonide kohta liiga palju arve","'"+origPol+"'"));
      return stack.pop();
   }
}